<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Recruitment\StoreRecruitmentRequest;
use App\Http\Requests\Backend\Recruitment\EditRecruitmentRequest;
use App\Http\Requests\Backend\Recruitment\UpdateRecruitmentRequest;
use App\Http\Requests\Backend\Recruitment\DeleteRecruitmentRequest;
use App\Models\Recruitment;
use App\Services\RecruitmentService;
use App\Services\CareerService;
use App\Services\AddressService;
use App\Services\DegreeService;
use App\Services\DepartmentService;
use App\Http\Controllers\Controller;

/**
 * Class RecruitmentController.
 */
class RecruitmentController extends Controller
{
    /**
     * @var RecruitmentService
     * @var CareerService
     * @var AddressService
     * @var DegreeService
     * @var DepartmentService
     */
    protected $recruitmentService;
    protected $careerService;
    protected $addressService;
    protected $degreeService;
    protected $departmentService;

    /**
     * RecruitmentController constructor.
     *
     * @param  RecruitmentService  $recruitmentService
     * @param  CareerService  $careerService
     * @param  AddressService $addressService
     * @param  DepartmentService $departmentService
     *
     */
    public function __construct(
        RecruitmentService $recruitmentService,
        CareerService $careerService,
        AddressService $addressService,
        DegreeService $degreeService,
        DepartmentService $departmentService
    )
    {
        $this->recruitmentService = $recruitmentService;
        $this->careerService = $careerService;
        $this->addressService = $addressService;
        $this->degreeService = $degreeService;
        $this->departmentService = $departmentService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.recruitment.index');
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('backend.recruitment.create')
            ->withDegrees($this->degreeService->getDegrees())
            ->withDepartments($this->departmentService->getDepartments())
            ->withCareers($this->careerService->getCareers())
            ->withAddresses($this->addressService->getAddresses());
    }

    /**
     * @param  StoreRecruitmentRequest  $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(StoreRecruitmentRequest $request)
    {
        $this->recruitmentService->store($request->all());

        return redirect()->route('admin.recruitment.index')->withFlashSuccess(__('The recruitment was successfully created.'));
    }

    /**
     * @param  EditRecruitmentRequest  $request
     * @param  Recruitment  $recruitment
     *
     * @return mixed
     */
    public function edit(EditRecruitmentRequest $request, Recruitment $recruitment)
    {
        return view('backend.recruitment.edit')
            ->withRecruitment($recruitment)
            ->withDegrees($this->degreeService->getDegrees())
            ->withDepartments($this->departmentService->getDepartments())
            ->withCareers($this->careerService->getCareers())
            ->withAddresses($this->addressService->getAddresses());
    }

    /**
     * @param  UpdateRecruitmentRequest  $request
     * @param  Recruitment  $recruitment
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateRecruitmentRequest $request, Recruitment $recruitment)
    {
        $this->recruitmentService->update($recruitment, $request->all());

        return redirect()->route('admin.recruitment.index')->withFlashSuccess(__('The recruitment was successfully updated.'));
    }

    /**
     * @param  DeleteRecruitmentRequest  $request
     * @param  Recruitment  $recruitment
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(DeleteRecruitmentRequest $request, Recruitment $recruitment)
    {
        $this->recruitmentService->destroy($recruitment);

        return redirect()->route('admin.recruitment.index')->withFlashSuccess(__('The recruitment was successfully deleted.'));
    }
}
