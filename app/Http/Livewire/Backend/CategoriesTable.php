<?php

namespace App\Http\Livewire\Backend;

use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\TableComponent;
use Rappasoft\LaravelLivewireTables\Traits\HtmlComponents;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class CategoriesTable.
 */
class CategoriesTable extends TableComponent
{
    use HtmlComponents;

    /**
     * @var string
     */
    public $sortField = 'name';

    /**
     * @var array
     */
    protected $options = [
        'bootstrap.container' => false,
        'bootstrap.classes.table' => 'table table-striped',
    ];

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return Category::whereIn('type', [Category::TYPE_NEW, Category::TYPE_LIBRARY])->orderBy('sort', 'asc');
    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make(__('Name'), 'name')
                ->searchable(function ($builder, $term) {
                    return $builder->whereTranslationLike('name', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('categories.*')->join('category_translations as t', function ($query) use($locale) {
                        $query->on('categories.id', '=', 't.category_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.name', $direction);
                })
                ->format(function (Category $model) {
                    return view('backend.category.includes.name', ['category' => $model]);
                }),
            Column::make(__('Description'), 'description')
                ->searchable(function ($builder, $term) {
                    return $builder->orWhereTranslationLike('description', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('categories.*')->join('category_translations as t', function ($query) use($locale) {
                        $query->on('categories.id', '=', 't.category_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.description', $direction);
                })
                ->format(function (Category $model) {
                    return view('backend.category.includes.description', ['category' => $model]);
                }),
            Column::make(__('Slug'), 'slug')
                ->searchable(function ($builder, $term) {
                    return $builder->orWhereTranslationLike('slug', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('categories.*')->join('category_translations as t', function ($query) use($locale) {
                        $query->on('categories.id', '=', 't.category_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.slug', $direction);
                })
                ->format(function (Category $model) {
                    return view('backend.category.includes.slug', ['category' => $model]);
                }),
            Column::make(__('Type'), 'type')
                ->sortable()
                ->format(function (Category $model) {
                    if ($model->type === Category::TYPE_NEW) {
                        return __('New');
                    }

                    if ($model->type === Category::TYPE_PROJECT) {
                        return __('Project');
                    }

                    if ($model->type === Category::TYPE_LIBRARY) {
                        return __('Library');
                    }

                    return 'N/A';
                }),
            Column::make(__('Status'), 'active')
                ->sortable()
                ->format(function (Category $model) {
                    if ($model->active === Category::ACTIVE) {
                        return __('Active');
                    }

                    if ($model->active === Category::INACTIVE) {
                        return __('Inactive');
                    }

                    return 'N/A';
                }),
            Column::make(__('Actions'))
                ->format(function (Category $model) {
                    return view('backend.category.includes.actions', ['category' => $model]);
                }),
        ];
    }
}
