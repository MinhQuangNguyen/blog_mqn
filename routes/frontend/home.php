<?php

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\AboutController;
// use App\Http\Controllers\Frontend\FieldController;
use App\Http\Controllers\Frontend\CourseController;
use App\Http\Controllers\Frontend\ProjectController;
use App\Http\Controllers\Frontend\NewsController;
use App\Http\Controllers\Frontend\DetailblogController;
// use App\Http\Controllers\Frontend\TermsController;
use App\Http\Controllers\Frontend\ContactController;
// use App\Http\Controllers\Frontend\RecruitmentController;
use App\Http\Controllers\Frontend\SearchController;
use Tabuna\Breadcrumbs\Trail;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */

Route::get('/', [HomeController::class, 'index'])
    ->name('index')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push(__('Home'), route('frontend.index'));
    });

Route::get('/404', function () { abort(404);});


Route::get('/gioi-thieu', [AboutController::class, 'index']);
Route::get('/about', [AboutController::class, 'index']);


// Route::get('/linh-vuc/{slug}', [FieldController::class, 'index']);
// Route::get('/field/{slug}', [FieldController::class, 'index']);
// Route::get('/linh-vuc', function () {
//     return redirect('/linh-vuc/bat-dong-san-cao-cap');
// });
// Route::get('/field', function () {
//     return redirect('/field/luxury-real-estate');
// });

Route::get('/tin-tuc/{categorySlug}', [NewsController::class, 'index']);
Route::get('/news/{categorySlug}', [NewsController::class, 'index']);
Route::get('/tin-tuc', function () {
    return redirect('/tin-tuc/tin-moi');
});
Route::get('/news', function () {
    return redirect('/news/news-feed');
});

Route::get('tin-tuc/{categorySlug}/{slug}.html', [NewsController::class, 'detail']);
Route::get('news/{categorySlug}/{slug}.html', [NewsController::class, 'detail']);

Route::get('/khoa-hoc/{categorySlug}', [ProjectController::class, 'index']);
Route::get('/couser/{categorySlug}', [ProjectController::class, 'index']);
Route::get('/khoa-hoc', function () {
    return redirect('/khoa-hoc/uiux');
});
Route::get('/couser', function () {
    return redirect('/couser/uiux');
});

// Route::get('/tuyen-dung', [RecruitmentController::class, 'index']);
// Route::get('/recruitment', [RecruitmentController::class, 'index']);

// Route::get('/tuyen-dung/list', [RecruitmentController::class, 'list']);
// Route::get('/recruitment/list', [RecruitmentController::class, 'list']);

// Route::get('/tuyen-dung/{slug}.html', [RecruitmentController::class, 'detail']);
// Route::get('/recruitment/{slug}.html', [RecruitmentController::class, 'detail']);

Route::get('/lien-he', [ContactController::class, 'index'])->name('lien-he.index');
Route::get('/contact', [ContactController::class, 'index'])->name('contact.index');
Route::post('/contact', [ContactController::class, 'store'])->name('contact.store');

Route::get('/tim-kiem', [SearchController::class, 'index'])->name('tim-kiem.index');
Route::get('/search', [SearchController::class, 'index'])->name('search.index');


Route::get('terms', [TermsController::class, 'index'])
    ->name('pages.terms')
    ->breadcrumbs(function (Trail $trail) {
        $trail->parent('frontend.index')
            ->push(__('Terms & Conditions'), route('frontend.pages.terms'));
    });
