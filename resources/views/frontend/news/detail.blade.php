@extends('frontend.layouts.alien')

@section('title', __('Chi tiết bài viết'))

@section('content')
@include('frontend.news.includes.topDetail')
@include('frontend.news.includes.detailBlog')
    {{-- tin tức --}}
@endsection
