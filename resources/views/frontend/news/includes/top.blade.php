<div class="top-header-content">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <div class="col-12 col-lg-6">
                <div class="content-left d-block d-lg-none"
                    style="background-image: url('/img/background-mobile.svg');">
                    <div class="title">Blog Alien Designer</div>
                    <div class="row">
                        <div class="col-xl-8">
                            {!! $setting['blog_alien'] !!}
                        </div>
                    </div>
                     <div class="moving-mouse-holder pt-0">
                        <div class="mouse">
                            <div class="mouse-button">&nbsp;</div>
                        </div>
                    </div>
                </div>
                <div class="content-left d-none d-lg-block"
                    style="background-image: url('/img/background-desktop-top.svg')">
                    <div class="title">Blog Alien Designer</div>
                    <div class="row">
                        <div class="col-xl-8">
                            {!! $setting['blog_alien'] !!}
                        </div>
                    </div>
                     <div class="moving-mouse-holder pt-0">
                        <div class="mouse">
                            <div class="mouse-button">&nbsp;</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="img-right-content-top-mobile d-block d-lg-none">
                    <div class="item-img-about">
                        <img src="/img/alien-list-blog.svg" />
                    </div>
                </div>
               <div class="img-right-content-top-desktop d-none d-lg-block">
                    <div class="item-img-about">
                        <div class="item-sao-thien-vuong">
                            <img src="/img/sao-hoa.svg" />
                        </div>
                        <div class="img-alien-ufo">
                            <img src="/img/alien-ufo.svg" alt="hinhanh">
                        </div>
                          <div class="img-alien-list-blog2">
                            <img src="/img/alien-list-blog2.svg" alt="hinhanh">
                        </div>
                        <div class="item-cloud-top-about-1">
                            <img src="/img/cloud-top-about-1.svg" alt="hinhanh">
                        </div>
                        <div class="item-cloud-top-about-2">
                            <img src="/img/cloud-top-about-2.svg" alt="hinhanh">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
