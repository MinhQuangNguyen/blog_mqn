@extends('frontend.layouts.alien')

@section('title', __('Course'))

@section('content')
@include('frontend.course.includes.top')
@include('frontend.course.includes.listCourse')
@endsection
