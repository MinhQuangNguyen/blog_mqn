<!-- banner hightlight -->
<?php $lang = app()->getLocale() === 'vi' ? '/du-an' : '/project'; ?>
<div class="best-project">
    <div class="container">
        <div class="title-group">
            <div class="title" style="font-size: 42px">@lang('The projects')</div>
            <div class="readmore-red readmore-project d-none d-lg-block">
                <x-utils.link :href="$lang" :text="__('View all')" />
            </div>
        </div>
        @if(isset($projects) && count($projects))
        <div class="list-banner-project-hightLight" x-data="component()" x-init="init()">
            <div class="row no-gutters">
                <div class="col-14 col-lg-10">
                    <div class="row no-gutters">
                        @foreach ($projects as $key => $item)
                            @if ($key > 1) @continue
                            @endif
                            <div class="col-14 col-lg-7" @mouseenter="setItem({{ $item }})" @mouseleave="setDefault()">
                                <div class="project-item">
                                    <div class="background" style="background-image: url({{ asset($item->image) }})">
                                    </div>
                                    <div class="project-item-title">{{ $item->name }}</div>
                                    <div class="backdrop"></div>
                                    <div class="backdrop-pc">
                                        <a
                                            href="{{ $lang . '/' . $item->category->slug . '/' . $item->slug . '.html' }}">
                                            <div class="backdrop-color"></div>
                                            <div class="backdrop-pc-title">{{ $item->name }}</div>
                                            <p class="detail-link">{{ __('Detail') }}</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row no-gutters">
                        @foreach ($projects as $key => $item)
                            @if ($key <= 1) @continue
                            @endif
                            <div class="col-14 col-lg-4-5" @mouseenter="setItem({{ $item }})" @mouseleave="setDefault()">
                                <div class="project-item">
                                    <div class="background square-lg"
                                        style="background-image: url({{ asset($item->image) }})">
                                    </div>
                                    <div class="project-item-title">{{ $item->name }}</div>
                                    <div class="backdrop"></div>
                                    <div class="backdrop-pc">
                                        <a
                                            href="{{ $lang . '/' . $item->category->slug . '/' . $item->slug . '.html' }}">
                                            <div class="backdrop-color"></div>
                                            <div class="backdrop-pc-title">{{ $item->name }}</div>
                                            <p class="detail-link">{{ __('Detail') }}</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-14 col-lg-4">
                    <div class="project-detail-content">
                        <div class="detail-title" x-text="item.name"></div>
                        <div class="detail-content" x-text="item.description"></div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
<!-- banner hightlingt -->

@push('after-scripts')
    <script type="text/javascript">
        function component() {
            return {
                item: {},
                setItem(item) {
                    this.item = item
                },
                setDefault() {
                    let item = @json($future);
                    this.item = item
                },
                init() {
                    let item = @json($future);
                    this.item = item
                },
            }
        }

    </script>
@endpush('after-scripts')
