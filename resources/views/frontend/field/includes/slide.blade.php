<!-- header slide -->
<div class="home-slide slide-kc slide-project" id="slide-field">
    <div id="home-slide-main" class="carousel slide  w-95" data-ride="carousel">
        <div class="carousel-inner">
            @if(isset($slide))
            <div class="carousel-item active">
                <div class="img" style="background-image: url({{ asset($slide->banner) }})"></div>
            </div>
            @endif
        </div>
    </div>

    <div class="title-group">
        @if(isset($slide))
        <div class="item padding-none active">
            <div class="item-slide-project">
                <div class="title">{{ $slide->name }}</div>
            </div>
        </div>
        @endif
    </div>
</div>
<!-- end header slide -->
