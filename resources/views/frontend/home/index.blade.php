@extends('frontend.layouts.alien')

@section('title', __('Home'))

@section('content')
@include('frontend.home.includes.top')
@include('frontend.home.includes.about')
@endsection
