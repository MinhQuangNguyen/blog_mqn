<!-- mobile -->
<div class="content-about-home d-block d-lg-none">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="content-about-right-mqn">
                    <div class="content-about-mqn">
                        <div class="title">tôi là ai?</div>
                        {!! $setting['toilaai'] !!}
                        <a href="/gioi-thieu" class="load-more">{{ __('Read more') }}</a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="img-content-about-mqn-mobile">
                    <img src="img/img2.svg" alt="hinhanh"/>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End mobile -->

<!-- Desktop -->
<div class="content-about-home d-none d-lg-block">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="img-content-about-mqn-desktop">
                    <div class="item-img-ufo">
                        <img src="img/ufo.svg" alt="hinhanh"/>
                    </div>
                    <div class="item-img-ufo-user">
                        <img src="img/user-ufo.svg" alt="hinhanh"/>
                    </div>
                    <div class="item-img-cloud-ufo">
                        <img src="img/cloud-ufo.svg" alt="hinhanh">
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="content-about-right-mqn">
                    <div class="content-about-mqn">
                        <div class="title">tôi là ai?</div>
                        <div class="row">
                            <div class="col-lg-8">
                                {!! $setting['toilaai'] !!}
                            </div>
                        </div>
                        <a href="/gioi-thieu" class="load-more">{{ __('Read more') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End desktop -->