<div class="top-header-content">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <div class="col-12 col-lg-6">
                <div class="content-left d-block d-lg-none" style="background-image: url('/img/background-mobile.svg');">
                    {!! $setting['intro_mb'] !!}
                    <a href="/tin-tuc" class="load-more">{{ __('Read more') }}</a>
                </div>
                <div class="content-left d-none d-lg-block" style="background-image: url('/img/background-desktop-top.svg')">
                    {!! $setting['intro'] !!}
                    <a href="/tin-tuc" class="load-more">{{ __('Read more') }}</a>
                </div>
            </div>
            
            <div class="col-12 col-lg-6">
                <div class="img-right-content-top-mobile d-block d-lg-none">
                    <img src="/img/img1.svg" alt="hinhanh"/>
                </div>
                <div class="img-right-content-top-desktop d-none d-lg-block">
                    <div class="item-user">
                        <img src="/img/home-mqn.svg" alt="hinhanh"/>
                    </div>
                    <div class="item-rocket">
                        <img src="/img/rocket.svg" alt="hinhanh">
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
