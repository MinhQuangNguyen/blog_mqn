<div class="detail-blog-main-alien-mqn">
    <div class="container-fluid">
        <div class="row d-flex justify-content-between">
            <div class="col-12 col-lg-8">
                <div class="item-content-info-detail-blog">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged. It was
                        popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of
                        Lorem Ipsum.</p>
                    <p>It is a long esta blished fact that a reader will be distracted by the readable content of a page
                        when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal
                        distri bution of letters, as opposed to using Content here, content here', making it look like
                        readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as
                        their default model text, and a search for ' lorem ipsum' will uncover many web sites still in
                        their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on
                        purpose (injected humour andthe like).</p>
                    <img src="/img/blogdt1.svg" alt="hinhanh">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged. It was
                        popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of
                        Lorem Ipsum.</p>
                    <p>It is a long esta blished fact that a reader will be distracted by the readable content of a page
                        when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal
                        distri bution of letters, as opposed to using Content here, content here', making it look like
                        readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as
                        their default model text, and a search for ' lorem ipsum' will uncover many web sites still in
                        their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on
                        purpose (injected humour andthe like).</p>
                </div>
                <div class="item-new-related">
                    <div class="title">bài viết liên quan</div>
                </div>

                <div class="partner-home-slide-group">
                    <div class="partner-home-slide">
                        <div class="item">
                            <div class="item-list-blog-mqn-main">
                                <a href="">
                                    <div class="item-blog-alien-mqn" style="background-image: url('/img/blog6.svg')">
                                    </div>
                                    <div class="content-info-title-blog-mqn">
                                        <div class="tag-blog">New</div>
                                        <div class="title-item-blog">
                                            <h1>khám phá những phong cách thiết kế mới trong năm 2021.</h1>
                                            <div class="row d-flex justify-content-between">
                                                <div class="col-12 col-lg-7 d-none d-lg-block">
                                                    <div class="item-icon-option">
                                                        <img src="/img/icon-alien.svg" alt="hinhanh">
                                                        <div class="text">Minh Quang Nguyen</div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-5">
                                                    <div class="item-icon-option">
                                                    <span><img src="/img/icon-clock.svg" alt="hinhanh"> 26/02/2021</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="item">
                            <div class="item-list-blog-mqn-main">
                                <a href="">
                                    <div class="item-blog-alien-mqn" style="background-image: url('/img/blog1.svg')">
                                    </div>
                                    <div class="content-info-title-blog-mqn">
                                        <div class="tag-blog">New</div>
                                        <div class="title-item-blog">
                                            <h1>khám phá những phong cách thiết kế mới trong năm 2021.</h1>
                                            <div class="row d-flex justify-content-between">
                                                <div class="col-12 col-lg-7 d-none d-lg-block">
                                                    <div class="item-icon-option">
                                                        <img src="/img/icon-alien.svg" alt="hinhanh">
                                                        <div class="text">Minh Quang Nguyen</div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-5">
                                                    <div class="item-icon-option">
                                                    <span><img src="/img/icon-clock.svg" alt="hinhanh"> 26/02/2021</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="item">
                            <div class="item-list-blog-mqn-main">
                                <a href="">
                                    <div class="item-blog-alien-mqn" style="background-image: url('/img/blog2.svg')">
                                    </div>
                                    <div class="content-info-title-blog-mqn">
                                        <div class="tag-blog">New</div>
                                        <div class="title-item-blog">
                                            <h1>khám phá những phong cách thiết kế mới trong năm 2021.</h1>
                                            <div class="row d-flex justify-content-between">
                                                <div class="col-12 col-lg-7 d-none d-lg-block">
                                                    <div class="item-icon-option">
                                                        <img src="/img/icon-alien.svg" alt="hinhanh">
                                                        <div class="text">Minh Quang Nguyen</div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-5">
                                                    <div class="item-icon-option">
                                                    <span><img src="/img/icon-clock.svg" alt="hinhanh"> 26/02/2021</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="btn-kc btn-back partner-prev">
                        <div class="btn-content"></div>
                    </div>
                    <div class="btn-kc btn-next partner-next">
                        <div class="btn-content"></div>
                    </div>
                </div>

            </div>

            <div class="col-12 col-lg-4">
                <div class="item-youtube-channer-alien">
                    <div class="title">dành cho người mới</div>
                    <div class="item-list-video-youtube">
                        <div class="item-video-youtube">
                            <a href="">
                                <span><img src="/img/iconXD.svg" alt="hinhanh">Sử dụng phần mềm adobe xd cơ bản</span>
                            </a>
                        </div>
                        <div class="item-video-youtube">
                            <a href="">
                                <span><img src="/img/iconXD.svg" alt="hinhanh">Sử dụng phần mềm adobe xd nâng cao</span>
                            </a>
                        </div>
                        <div class="item-video-youtube">
                            <a href="">
                                <span><img src="/img/iconXD.svg" alt="hinhanh">Sử dụng prototype trong adobe xd</span>
                            </a>
                        </div>
                        <div class="item-video-youtube">
                            <a href="">
                                <span><img src="/img/iconPTS.svg" alt="hinhanh">Sử dụng phần mềm adobe photoshop</span>
                            </a>
                        </div>
                        <div class="item-video-youtube">
                            <a href="">
                                <span><img src="/img/iconPTS.svg" alt="hinhanh">Hướng dẫn chỉnh màu trong photoshop</span>
                            </a>
                        </div>
                        <div class="item-video-youtube">
                            <a href="">
                                <span><img src="/img/iconAI.svg" alt="hinhanh">Sử dụng phần mềm adobe Illustrator</span>
                            </a>
                        </div>
                        <div class="item-video-youtube">
                            <a href="">
                                <span><img src="/img/iconAI.svg" alt="hinhanh">Hướng dẫn thiết kế logo dạng chữ</span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="item-behance-mqn-main">
                    <div class="title">dự án mới trên <i class='fab fa-behance'></i></div>
                    <div class="item-project-behance">
                        <div class="iframely-embed">
                            <div class="iframely-responsive" style="padding-bottom: 75%; padding-top: 120px;"><a
                                    href="https://www.behance.net/gallery/111829375/App-Booking-Travel"
                                    data-iframely-url="//cdn.iframe.ly/zWcCZ3P"></a></div>
                        </div>
                    </div>

                    <div class="item-project-behance">
                        <div class="iframely-embed">
                            <div class="iframely-responsive" style="padding-bottom: 75%; padding-top: 120px;"><a
                                    href="https://www.behance.net/gallery/113845241/App-Order-Fast-Food-Pizza"
                                    data-iframely-url="//cdn.iframe.ly/rMnTVje"></a></div>
                        </div>
                    </div>
                </div>

                <div class="let-get-social-mqn-main">
                    <div class="title">Let's get social</div>
                    <div class="icon-social-form">
                        <ul>
                            <li>
                                <a href="">
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x icon-background1"></i>
                                        <i class="fab fa-facebook-f fa-stack-1x color-white"></i>
                                    </span>
                                </a>
                            </li>

                            <li>
                                <a href="">
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x icon-background1"></i>
                                        <i class="fab fa-behance fa-stack-1x color-white"></i>
                                    </span>
                                </a>
                            </li>

                            <li>
                                <a href="">
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x icon-background1"></i>
                                        <i class="fab fa-youtube fa-stack-1x color-white"></i>
                                    </span>
                                </a>
                            </li>

                            <li>
                                <a href="">
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x icon-background1"></i>
                                        <i class="fab fa-instagram fa-stack-1x color-white"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>
