<div class="top-header-content">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <div class="col-12 col-lg-6">
                <div class="content-left d-block d-lg-none top-70-mobile"
                    style="background-image: url('/img/background-mobile.svg');">
                    <div class="row">
                        <div class="col-7 col-lg-7">
                            <div class="item-icon-option">
                                <img src="/img/icon-alien.svg" alt="hinhanh">
                                <div class="text">Minh Quang Nguyen</div>
                            </div>
                        </div>
                        <div class="col-5 col-lg-5">
                            <div class="item-icon-option">
                                <img src="/img/icon-clock.svg" alt="hinhanh">
                                <div class="text">26/02/2021</div>
                            </div>
                        </div>
                    </div>
                    <h2>Các "Tip" hỗ trợ trong việc thiết kế website thương hiệu 2021</h2>
                    <div class="moving-mouse-holder">
                        <div class="mouse">
                            <div class="mouse-button">&nbsp;</div>
                        </div>
                    </div>
                </div>
                <div class="content-left d-none d-lg-block"
                    style="background-image: url('/img/background-desktop-top.svg')">
                    <div class="content-title-detail-blog-main">
                        <div class="row d-flex justify-content-between">
                            <div class="col-7 col-lg-6">
                                <div class="item-icon-option">
                                    <img src="/img/icon-alien.svg" alt="hinhanh">
                                    <div class="text">Minh Quang Nguyen</div>
                                </div>
                            </div>
                            <div class="col-5 col-lg-5">
                                <div class="item-icon-option">
                                    <img src="/img/icon-clock.svg" alt="hinhanh">
                                    <div class="text">26/02/2021</div>
                                </div>
                            </div>
                        </div>
                        <h2>Các "Tip" hỗ trợ trong việc thiết kế website thương hiệu 2021</h2>
                        <div class="moving-mouse-holder">
                            <div class="mouse">
                                <div class="mouse-button">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 d-none d-lg-block">
            <div class="img-right-content-top-desktop d-none d-lg-block">
                    <div class="item-img-about">
                        <div class="item-alien-trai-dat">
                            <img src="/img/alien-traidat.svg" />
                        </div>
                        <div class="img-ufo-sao-hoa">
                            <img src="/img/ufo-sao-hoa.svg" alt="hinhanh">
                        </div>
                        <div class="item-cloud-top-about-1 bt6">
                            <img src="/img/cloud-top-about-1.svg" alt="hinhanh">
                        </div>
                        <div class="item-cloud-top-about-2 bt15">
                            <img src="/img/cloud-top-about-2.svg" alt="hinhanh">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
