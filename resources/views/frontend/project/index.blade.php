@extends('frontend.layouts.alien')

@section('title', __('Khóa học'))

@section('content')
    @include('frontend.project.includes.top')
    @include('frontend.project.includes.listCourse', $projects)
    {{-- dự án --}}
@endsection
