<div class="row">
    @foreach($projects as $project)
        <div class="col-12 col-lg-4">
            <div class="item-list-courser">
                <div class="item-courser">
                    <a href="{{ $project->slug }}" target="_blank">
                        <div class="item-courser-avata" style="background-image: url('{{ asset($project->image) }}')">
                            <div class="item-timeline-courser">
                              <img src="/img/youtube-play.svg" alt="hinhanh">
                            </div>
                        </div>
                        <div class="item-info-detail-courser">
                            <div class="title">{{ $project->name }}</div>
                            <div class="text">{{ $project->description }}</div>
                            <div class="by">Tác giả : {{ $project->address }}</div>
                            <div class="icon-view-courser">
                                <div class="row">
                                    <div class="col-7 col-lg-8">
                                        <ul>
                                            <li><img src="/img/star.svg" alt="hinhanh"></li>
                                            <li><img src="/img/star.svg" alt="hinhanh"></li>
                                            <li><img src="/img/star.svg" alt="hinhanh"></li>
                                            <li><img src="/img/star.svg" alt="hinhanh"></li>
                                            <li><img src="/img/star.svg" alt="hinhanh"></li>
                                        </ul>
                                    </div>
                                    <div class="col-5 col-lg-4">
                                        <div class="view">{{ __('WATCH NOW') }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    @endforeach

    <div class="col-12 col-lg-12">
        {{ $projects->fragment('couser-srcoll')->links() }}
    </div>
</div>
