<div class="item-list-course-main-mqn" id="couser-srcoll">
    <div class="container-fluid">
        <div class="row d-flex justify-content-between">
            <div class="col-12 col-lg-6">
                <div class="title-list-blog">
                    <div class="title">{{ __('Course') }}</div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="list-menu-tags">
                    <ul class="nav nav-tabs">
                        @foreach($categories as $category)
                        <?php $link = 'khoa-hoc/'.$category->slug ?>
                        <li class="nav-item {{ request()->is($link) || request()->is($link.'/*') ? 'active': '' }}">
                            <a class="nav-link" href="/khoa-hoc/{{$category->slug}}#couser-srcoll">{{$category->name}}</a>
                        </li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>

        <div class="tab-content">
            @include('frontend.project.includes.uiux')
        </div>
    </div>
</div>



@push('after-scripts')
<script type="text/javascript">
    $(document).ready(function () {
        var url_string = window.location.href;
        if (new RegExp('\#couser-srcoll').test(url_string)) {
            $('html, body').animate({
                scrollTop: $("#couser-srcoll").offset().top - 100
            });
        }
    })
</script>
@endpush