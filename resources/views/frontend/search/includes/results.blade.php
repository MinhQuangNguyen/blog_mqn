<?php $lang = app()->getLocale() === 'vi' ? '/tim-kiem' : '/search'; ?>

<div class="row d-flex justify-content-center" id="results-scroll">
    <div class="col-12 col-lg-12">
        <div class="title-list-blog">
            <div class="title text-center"> Từ Khóa &nbsp;<span>"{{$keyword}}"</span> có
                <span>"{{$resultLength}}"</span> kết quả</div>
        </div>
    </div>
</div>

<div class="item-list-course-main-mqn results-couser">
    <div class="container-fluid">
        <div class="row">
            @if(isset($projects))
            @foreach($projects as $key => $result)
            <div class="col-12 col-lg-4">
                <div class="item-list-courser">
                    <div class="item-courser">
                        <a href="{{ $result->slug }}" target="_blank">
                            <div class="item-courser-avata"
                                style="background-image: url('{{ asset($result->image) }}')">
                                <div class="item-timeline-courser">
                                    <img src="/img/youtube-play.svg" alt="hinhanh">
                                </div>
                            </div>
                            <div class="item-info-detail-courser">
                                <div class="title">{{ $result->name }}</div>
                                <div class="text">{{ $result->description }}</div>
                                <div class="by">Tác giả : {{ $result->address }}</div>
                                <div class="icon-view-courser">
                                    <div class="row">
                                        <div class="col-8">
                                            <ul>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                                <li><img src="/img/star.svg" alt="hinhanh"></li>
                                            </ul>
                                        </div>
                                        <div class="col-4">
                                            <div class="view">Xem ngay</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</div>

<div class="list-blog-main-mqn results-blog">
    <div class="container-fluid">
        <div class="row">
            @if(isset($posts))
            @foreach($posts as $key => $result)
            <div class="col-12 col-lg-4">
                <div class="item-list-blog-mqn-main">
                    <a href="/tin-tuc/{{  $result->category->slug.'/'. $result->slug.'.html' }}">
                        <div class="item-blog-alien-mqn" style="background-image: url('{{ asset( $result->image) }}')">
                        </div>
                        <div class="content-info-title-blog-mqn">
                            <div class="tag-blog">{{  $result->tagser }}</div>
                            <div class="title-item-blog">
                                <h1>{{  $result->title }}</h1>
                                <div class="row d-flex justify-content-between">
                                    <div class="col-12 col-lg-7 d-none d-lg-block">
                                        <div class="item-icon-option">
                                            <img src="/img/icon-alien.svg" alt="hinhanh">
                                            <div class="text">{{  $result->author }}</div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-5">
                                        <div class="item-icon-option">
                                            <span><img src="/img/icon-clock.svg" alt="hinhanh">{{ date_format($result->created_at, 'd/m/Y') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</div>


@push('after-scripts')
<script type="text/javascript">
    $(document).ready(function () {
        var url_string = window.location.href;
        if (!new RegExp('\#results-scroll').test(url_string)) {
            $('html, body').animate({
                scrollTop: $("#results-scroll").offset().top - 150
            }, 100);
        }
    })

</script>
@endpush
