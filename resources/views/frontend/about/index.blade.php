@extends('frontend.layouts.alien')

@section('title', __('Giới thiệu'))

@section('content')
@include('frontend.about.includes.top')
@include('frontend.about.includes.aboutBlog')
@endsection
