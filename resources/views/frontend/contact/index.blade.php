@extends('frontend.layouts.alien')

@section('title', __('Contact'))

@section('content')
@include('frontend.contact.includes.top')
@include('frontend.contact.includes.form')
@endsection
