<div id="contact-scroll">
    <div class="item-form-contact-main">
        <div class="container-fluid">
            <div class="row">
                <div id="MqnContact" class="col-12 col-lg-6">
                    <div class="item-img-form-contact-mqn">
                        <img src="/img/img6.svg" />
                    </div>
                </div>

                <div id="FormContactMqn" class="col-12 col-lg-6">
                    <div class="form-contact">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif

                        <div class="title">{{ __('Contact me') }}</div>
                        <x-forms.post :action="route('frontend.contact.store')">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="name" placeholder="{{ __('Name') }}" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <input type="number" class="form-control" name="phone" placeholder="{{ __('Phone number') }}" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="company" placeholder="{{ __('Company') }}">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email" placeholder="{{ __('Email') }}" required>
                                    </div>
                                </div>
                                <div class="col-12">
                                <div class="form-group">
                                    <textarea class="form-control" rows="6" name="content" placeholder="{{ __('Content') }}"></textarea>
                                </div>
                                </div>
                            </div>
                            <div class="row justify-content-between">
                                <div class="col-7 col-lg-8">
                                    <div class="icon-social-form">
                                        <ul>
                                            <li>
                                                <a href="{!! $setting['link_facebook'] !!}" target="_blank">
                                                    <span class="fa-stack fa-lg">
                                                        <i class="fa fa-circle fa-stack-2x icon-background1"></i>
                                                        <i class="fab fa-facebook-f fa-stack-1x color-white"></i>
                                                    </span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="{!! $setting['link_behance'] !!}" target="_blank">
                                                    <span class="fa-stack fa-lg">
                                                        <i class="fa fa-circle fa-stack-2x icon-background1"></i>
                                                        <i class="fab fa-behance fa-stack-1x color-white"></i>
                                                    </span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="{!! $setting['link_youtube'] !!}" target="_blank">
                                                    <span class="fa-stack fa-lg">
                                                        <i class="fa fa-circle fa-stack-2x icon-background1"></i>
                                                        <i class="fab fa-youtube fa-stack-1x color-white"></i>
                                                    </span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="{!! $setting['link_ins'] !!}" target="_blank">
                                                    <span class="fa-stack fa-lg">
                                                        <i class="fa fa-circle fa-stack-2x icon-background1"></i>
                                                        <i class="fab fa-instagram fa-stack-1x color-white"></i>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="text">Let's get social</div>
                                </div>
                                <div class="col-5 col-lg-4">
                                    <button class="btn btn-outline-secondary" type="submit"><i
                                            class="fas fa-paper-plane"></i> {{ __('feedback') }}</button>
                                </div>
                            </div>
                        </x-forms.post>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div>



@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            var url_string = window.location.href;
            setTimeout(() => {
                if (new RegExp('\#contact-scroll').test(url_string)) {
                    $('html, body').animate({
                        scrollTop: $("#contact-scroll").offset().top - 0
                    }, 100);
                }
            }, 1000)
        })
    </script>
@endpush
