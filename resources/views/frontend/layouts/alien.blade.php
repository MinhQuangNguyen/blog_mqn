<!doctype html>
<html lang="{{ htmlLang() }}" @langrtl dir="rtl" @endlangrtl>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Website {{ appName() }} | @yield('title')</title>
    <meta name="author" content="@yield('meta_author', 'Minh Quang Nguyen')">

    <meta property="og:locale" content="{{ htmlLang() }}" />
    <meta property="og:title" content="@yield('title', appName())" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ request()->fullUrl() }}" />
    <meta property="og:description" name="description" content="Webiste Blog chia sẻ kiến thức thiết kế đồ họa và khóa học thiết kế đồ họa online miễn phí." />
    <meta property="og:site_name" content="{{ appURL() }}" />
    <meta property="og:image" content="/img/meta.png" />
    <link rel="icon" sizes="94x94" type="image/gif" href="/img/flaticon.png"/>
    @yield('meta')

    @stack('before-styles')
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="{{ mix('css/frontend.css') }}" rel="stylesheet">
    <livewire:styles />
    @stack('after-styles')

</head>

<body>

    <div id="app">
        @include('frontend.includes.header', ['menu' => $menu])

        {{-- @include('includes.partials.messages') --}}

        <main>
            @yield('content')
        </main>
        @include('frontend.includes.footer')
    </div>
    <!--app-->

    @stack('before-scripts')
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/frontend.js') }}"></script>
    <livewire:scripts />
    @include('frontend.includes.js')
    @stack('after-scripts')
</body>

</html>
