<div class="slide-text">
    <div class="row no-gutters">
        <div class="col-14 col-lg-10">
            <div class="slide-text-left">
                <div class="btn-kc btn-back">
                    <div class="btn-content"></div>
                </div>
                <div class="btn-kc btn-next">
                    <div class="btn-content"></div>
                </div>
                <div class="row">
                    <div class="col-3 col-sm-2 col-lg-2">
                        <div class="slide-text-icon">
                            <img src="{{ asset('img/logoMain.svg') }}" alt="hinhanh">
                        </div>
                    </div>
                    <div class="col-9 col-sm-10 col-lg-10">
                        <div class="slide-text-left-group">
                            <div class="slide-text-left-title">
                                <div class="item-title-wrap">
                                    @foreach($slidesT as $slide)
                                        <div class="item" data-href="{{ $slide->link }}">{{ $slide->title }}</div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="slide-text-left-content">
                                <div class="item-content-wrap">
                                    @foreach($slidesT as $slide)
                                        <div class="item">{!! $slide->content !!}</div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="slide-text-left-number">
                                        <div class="number-old">01</div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="readmore-wrap">
                                        <div class="readmore readmore-red">
                                            <a href="">{{ __('Read more') }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- do not edit this content --}}
        <div class="col-14 col-lg-4">
            <div class="slide-text-right">
                <div class="slide-text-right-title"></div>
                <div class="slide-text-right-number">
                    <div class="number-old"></div>
                </div>
            </div>
        </div>
        {{-- do not edit this content --}}
    </div>
</div>
