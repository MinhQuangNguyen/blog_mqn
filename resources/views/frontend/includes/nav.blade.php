<div class="header-menu">
    <nav class="navbar navbar-expand-lg navbar-light justify-content-between">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <div class="logo">
                    <img src="{{ asset('img/logo.svg') }}" alt="hinhanh"/>
                </div>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-mobile"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse menu right-menu">
                <ul class="navbar-nav">
                    @if(isset($menu))
                    @foreach ($menu as $menuItem)
                        @if ($menuItem->hasChild())
                            <li class="nav-item dropdown {{ request()->is((strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link)) || request()->is((strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link)."/*") ? 'active': ''}}">
                                <a class="nav-link" href="{{$menuItem->clickable ? $menuItem->link: '#'}}">
                                    {{ $menuItem->name }}
                                </a>
                            </li>
                        @else
                        <?php $link = strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link ?>
                        <li class="nav-item {{ request()->is($link) || request()->is($link.'/*') ? 'active': '' }}">
                            <a class="nav-link" href="{{ $menuItem->link }}">
                                {{ $menuItem->name }}
                            </a>
                        </li>
                        @endif
                    @endforeach
                    @endif
                    <li class="nav-item">
                        <div class="nav-link seacrh-mqn none" data-toggle="modal" data-target="#exampleModal">
                            <img src="/img/icon-search.svg" alt="hinhanh">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- <div class=" d-none d-lg-block">
            @include("frontend.includes.lang-search")
        </div> -->
    </nav>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <i class="fas fa-times-circle" data-dismiss="modal"></i>
                <div class="form-search">
                    <?php $lang = (app()->getLocale() === 'vi' ? 'tim-kiem' : 'search') ?>
                    <form action="{{ '/'.$lang }}" method="get" class="form-inline">
                        <div class="row">
                            <div class="col-12">
                                <input type="text" id="tags" name="k" value="{{ request()->get('k') }}" placeholder="Nhập từ khóa tìm kiếm..." autocomplete="on" required>
                            </div>
                        </div>
                        <button type="submit" class="mb-2"><img src="/img/icon-search-white.svg" alt=""></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@include("frontend.includes.menu-mobile")
