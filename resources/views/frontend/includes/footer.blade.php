<div class="footer-main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-7">
                <div class="info-footer-left">
                    <div class="logo">
                        <img src="/img/logo.svg" alt="hinhanh">
                    </div>
                    <div class="item-contact">
                        <img src="/img/icon-map.svg" alt="hinhanh">
                        <p>{!! $setting['address'] !!}</p>
                    </div>
                    <div class="item-contact">
                        <img src="/img/icon-gmail.svg" alt="hinhanh">
                        <p>{!! $setting['email'] !!}</p>
                    </div>
                    <div class="item-contact">
                        <img src="/img/icon-phone.svg" alt="hinhanh">
                        <p>{!! $setting['tel'] !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-5">
                <div class="item-form-regsiter-right">
                    <div class="text">{{ __('Sign up to receive the latest articles!') }}</div>
                    <form action="" method="">
                        <div class="input-group mb-3">
                            <input type="email" class="form-control" placeholder="{{ __('Email') }}" required>
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-paper-plane"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            <div class="item-line d-none d-lg-block"></div>

            <div class="col-12 col-lg-7">
                <div class="item-menu-footer-left">
                    <ul>
                        <li>
                            <a href="" class="readmore-dark">{{ __('Provision') }}</a>
                        </li>
                        <li>
                            <a href="" class="readmore-dark">{{ __('Policy') }}</a>
                        </li>
                        <li>
                            <a href="/lien-he#contact-scroll" class="readmore-dark">{{ __('Advertising support') }}</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="item-line d-block d-lg-none"></div>

            <div class="col-12 col-lg-5">
                <div class="copyright-mqn">
                    <div class="text">{{ __('© Alien Designer 2021. All rights reserved.') }}</div>
                </div>
            </div>

            <div class="scroll-top-main-mqn">
                <a href="#" id="toTopBtn" class="cd-top text-replace js-cd-top cd-top--is-visible cd-top--fade-out" data-abc="true">
                       <img src="/img/icon-top.svg" alt="hinhanh">
                </a>
            </div>

            {{-- <div class="lang-phi-thuyen">
                <div class="item-phi-thuyen-lang">
                    <img src="/img/phi-thuyen-lang.svg" alt="hinhanh">
                </div>
                <div class="item-lang">
                   <a href="/lang/vi"><img src="/img/vi.svg" alt="hinhanh"></a>
                </div>
                <div class="item-lang">
                    <a href="/lang/en"><img src="/img/en.svg" alt="hinhanh"></a>
                </div>
            </div> --}}
        </div>
    </div>
</div>


@push('after-scripts')
<script type="text/javascript">
var btn = $('#toTopBtn');

$(window).scroll(function() {
  if ($(window).scrollTop() > 50) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '50');
});


</script>
@endpush