@extends('backend.layouts.app')

@section('title', __('Post Management'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Post Management')
        </x-slot>

        @if ($logged_in_user->hasAllAccess())
            <x-slot name="headerActions">
                <x-utils.link
                    icon="c-icon cil-plus"
                    class="card-header-action"
                    :href="route('admin.post.create')"
                    :text="__('Create Post')"
                />
            </x-slot>
        @endif

        <x-slot name="body">
            <livewire:backend.posts-table />
        </x-slot>
    </x-backend.card>
@endsection
