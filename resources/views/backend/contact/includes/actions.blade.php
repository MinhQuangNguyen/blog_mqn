<x-utils.detail-button :href="route('admin.contact.detail', $contact)" />
<x-utils.delete-button :href="route('admin.contact.destroy', $contact)" />
