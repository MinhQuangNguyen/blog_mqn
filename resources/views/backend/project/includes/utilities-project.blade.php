<div class="card">
    <h5 class="card-header">
        <a data-toggle="collapse" href="#collapse-utility" aria-expanded="true" aria-controls="collapse-utility" id="heading-utility" class="d-block">
            <i class="fa fa-chevron-down float-right"></i>
            @lang('Utilities Project')
        </a>
    </h5>
    <div id="collapse-utility" class="collapse show" aria-labelledby="heading-utility">
        <div class="card-body">
            <div class="form-group row">
                <label for="title" class="col-md-2 col-form-label">@lang('Title')</label>

                <div class="col-md-10">
                    <input type="text" name="utilities[title]" class="form-control" placeholder="{{ __('Title') }}" value="{{ old('utilities.title') ?? $project->utilities['title'] ?? __('Utilities Project') }}" maxlength="100" required />
                </div>
            </div>

            @if(!isset($project))
            <div class="form-group row">
                <label for="title_en" class="col-md-2 col-form-label">@lang('Title (En)')</label>

                <div class="col-md-10">
                    <input type="text" name="utilities[title_en]" class="form-control" placeholder="{{ __('Title (En)') }}" value="{{ old('utilities.title_en') ?? 'Utilities' }}" maxlength="100" required />
                </div>
            </div>
            @endif

            <div class="form-group row">
                <label for="content" class="col-md-2 col-form-label">@lang('Content')</label>

                <div class="col-md-10">
                    <textarea type="text" name="utilities[content]" id="content_p" class="form-control" placeholder="{{ __('Content') }}">{{ old('utilities.content') ?? (isset($project) && isset($project->utilities['content']) ? $project->utilities['content'] : null) }}</textarea>
                </div>
            </div>

            @if(!isset($project))
            <div class="form-group row">
                <label for="content_en" class="col-md-2 col-form-label">@lang('Content (En)')</label>

                <div class="col-md-10">
                    <textarea type="text" name="utilities[content_en]" id="content_p_en" class="form-control" placeholder="{{ __('Content (En)') }}">{{ old('utilities.content_en') }}</textarea>
                </div>
            </div>
            @endif

            @include('backend.project.includes.images', ['type' => 'utilities', 'project' => isset($project) ? $project : null])
        </div>
    </div>
</div>
