@extends('backend.layouts.app')

@section('title', __('Create Introduce'))

@section('content')
    <x-forms.post :action="route('admin.introduce.store')">
        <x-backend.card>
            <x-slot name="header">
                @lang('Create Introduce')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.introduce.index')" :text="__('Back')" />
            </x-slot>

            <x-slot name="body">
                <div>
                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label">@lang('Name')</label>

                        <div class="col-md-10">
                            <input type="text" name="name" class="form-control" placeholder="{{ __('Name') }}" value="{{ old('name') }}" maxlength="100" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name_en" class="col-md-2 col-form-label">@lang('Name (En)')</label>

                        <div class="col-md-10">
                            <input type="text" name="name_en" class="form-control" placeholder="{{ __('Name (En)') }}" value="{{ old('name_en') }}" maxlength="100" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="link" class="col-md-2 col-form-label">@lang('Link')</label>

                        <div class="col-md-10">
                            <input type="text" name="link" class="form-control" placeholder="{{ __('Link') }}" value="{{ old('link') }}" maxlength="100" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="link_en" class="col-md-2 col-form-label">@lang('Link (En)')</label>

                        <div class="col-md-10">
                            <input type="text" name="link_en" class="form-control" placeholder="{{ __('Link (En)') }}" value="{{ old('link_en') }}" maxlength="100" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="sort" class="col-md-2 col-form-label">@lang('Sort')</label>

                        <div class="col-md-10">
                            <input type="number" name="sort" class="form-control" placeholder="{{ __('Sort') }}" value="{{ old('sort') }}" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="content" class="col-md-2 col-form-label">@lang('Content')</label>

                        <div class="col-md-10">
                            <textarea type="text" name="content" id="content" class="form-control" placeholder="{{ __('Content') }}">{{ old('content') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="content_en" class="col-md-2 col-form-label">@lang('Content (En)')</label>

                        <div class="col-md-10">
                            <textarea type="text" name="content_en" id="content_en" class="form-control" placeholder="{{ __('Content (En)') }}">{{ old('content_en') }}</textarea>
                        </div>
                    </div>

                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Create')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.post>

    @push('after-scripts')
    <script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('js/ckeditor/ckeditor_replace.js')}}"></script>
    @endpush('after-scripts')
@endsection
