require('./components/slide-text')
require('./components/menu-vertical')
require('./components/slide-recrument')

function header() {
    //click button search
    $(".search-btn").click(function() {
        $(".search-inp").toggleClass("active");
        $(".navbar-collapse.menu").toggleClass("onSearch")
        headerMenu()
    })
}

function headerMenu() {
    //change margin of menu
    setTimeout(function() {
        const containerWidth = $(".right-menu").offset().left - (
            $(".logo").offset().left + $(".logo").outerWidth()
        )

        const menuWidth = $(".menu .navbar-nav").width();
        const margin = (containerWidth - menuWidth) / 2;
        // console.log(containerWidth, menuWidth, margin)
        $(".menu").css({
            marginLeft: $(".logo").outerWidth() + margin - 45 + "px"
        })
    }, 300)

}

function homeSlide() {
    $('.slide-kc .carousel').on('slide.bs.carousel', function(e) {
        const index = e.to;
        $(this).parents(".slide-kc").find(".title-group .active").removeClass("active")
        $($(this).parents(".slide-kc").find(".title-group .item")[index]).addClass("active")

    })
}

function partnerSlide() {
    $(".partner-home-slide").slick({
        dots: false,
        infinite: true,
        mobileFirst: true,
        // variableWidth: true,
        speed: 300,
        slidesToShow: 1,
        prevArrow: $('.partner-prev'),
        nextArrow: $('.partner-next'),

        responsive: [{
                breakpoint: 767,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    })
}

function homeScopeHover() {
    $(".project-item").hover(function() {
        const bottomLine = $(this).find(".bottom-line");
        const bottomLineWidth = $(bottomLine).width();
        const bottomHoverWidth = $(bottomLine).attr("data-width-hover");
        $(bottomLine).attr("data-width", bottomLineWidth).width(bottomLineWidth + Number(bottomHoverWidth))
    }, function() {
        const bottomLine = $(this).find(".bottom-line");
        const bottomWidth = $(bottomLine).attr("data-width");
        $(bottomLine).width(bottomWidth)
    })
    $(window).resize(function() {
        $(".project-item .bottom-line").removeAttr("data-width").removeAttr("style");
    })
}

function menuMobile() {
    //click submenu
    $(".nav-item.has-sub-menu").click(function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active").find(".sub-menu").stop(true, true).slideUp();
        } else {
            $(this).addClass("active").find(".sub-menu").stop(true, true).slideDown();
        }
    })

    //click show menu mobile
    $(".navbar-toggler").click(function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $("html,body").removeClass("no-scroll");
            $(".menu-mobile").stop(true, true).fadeOut({
                duration: 300
            });
        } else {
            $(this).addClass("active")
            $("html,body").addClass("no-scroll");
            $(".menu-mobile").stop(true, true).fadeIn({
                duration: 300
            });
        }
    })
    $(window).resize(function () {
        $(this).removeClass("active")
        $(".menu-mobile").stop(true, true).fadeOut({
            duration: 300
        });
    });
}


function changeTabField() {
    //active lại tab theo url
    const $fieldPage = $(".tab-field-main");
    if ($fieldPage.length) {
        const pathName = window.location.pathname;
        $fieldPage.find(`.nav-tabs .nav-item[data-url='${pathName}']`).tab("show")
    }
    //điều chỉnh lại hover trên menu và slide.
    $(".tab-field-main").off("click").on("click", ".nav-item", function() {
        const newBanner = $(this).data("banner");
        const newName = $(this).data("name");
        const newLink = $(this).data("url");
        const navActive = $('.nav-item.dropdown.active')
        if (newLink) {
            $("#slide-field").find('.img').css('background-image', 'url(' + newBanner + ')')
            $("#slide-field").find('.title').text(newName)
            navActive.find('.active').removeClass('active')
            navActive.find(`[data-href='${newLink}']`).addClass('active')
            window.history.pushState('', '', newLink);
        }
    })
}




// End scroll top

// Scroll change logo
$(function() {
    $(window).scroll(function() {
        if ($(this).scrollTop() > 20) {
            $('.seacrh-mqn img').attr('src', '/img/icon-search-white.svg');
        } else {
            $('.seacrh-mqn img').attr('src', '/img/icon-search.svg');
        }
    })
});
// End scroll change logo


//header menu change background
$(window).scroll(function() {
    $('nav').toggleClass('scrolled', $(this).scrollTop() > 150);
});
// End header menu change background

$("document").ready(function() {
    header();
    headerMenu();
    homeSlide();
    menuMobile();
    homeScopeHover();
    partnerSlide();
    changeTabField();
})

$(window).resize(function() {
    headerMenu()
})