/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import 'alpinejs'
window.moment = require('moment');
window._ = require('lodash');
window.mediumZoom =  require('medium-zoom').default;

require('../bootstrap');
require('../plugins');
require('select2');

require('./alien')
