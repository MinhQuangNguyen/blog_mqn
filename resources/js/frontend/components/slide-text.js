function slideText() {
    $(".slide-text").each(function () {
        const $parent = $(this);
        const itemLength = $parent.find(".slide-text-left-title .item-title-wrap .item").length;
        const viewLeftWidth = $parent.find(".slide-text-left-title").width();
        const viewHeight = $parent.find(".slide-text-left-title").height();

        $parent.find(".slide-text-left-content .item-content-wrap").width(itemLength * viewLeftWidth);
        $parent.find(".slide-text-left-content .item-content-wrap .item").width(viewLeftWidth);


        $parent.find(".slide-text-left-number").css({
            height: $parent.find(".slide-text-left-number").removeAttr("style").outerHeight()
        });


        $parent.find(".slide-text-right-title").html($parent.find(".slide-text-left-title .item-title-wrap").clone())
        $parent.find(".slide-text-right-title .item-title-wrap").append($parent.find(
            ".slide-text-right-title .item-title-wrap .item").first())
        const viewRightWidth = $parent.find(".slide-text-right-title").width();
        $parent.find(".slide-text-right-title .item-title-wrap").width(itemLength * viewRightWidth);
        $parent.find(".slide-text-right-title .item-title-wrap .item").width(viewRightWidth);
        move(0, 1)
        $(this).find(".btn-next").off("click").on("click", function () {
            if ($parent.attr("data-scrolling") == '1') {
                return false;
            }
            let index = Number($parent.attr("data-index")) || 0

            //tăng index.
            if (index >= itemLength - 1) index = 0;
            else index++;

            let nextIndex = index + 1;
            if (nextIndex >= itemLength) nextIndex = 0;

            move(index, nextIndex)
        })

        $(this).find(".btn-back").off("click").on("click", function () {
            if ($parent.attr("data-scrolling") == '1') {
                return false;
            }
            let index = Number($parent.attr("data-index")) || 0

            //giảm index.
            if (index == 0) index = itemLength - 1;
            else index--;

            let nextIndex = index + 1;
            if (nextIndex >= itemLength) nextIndex = 0;

            move(index, nextIndex)
        })

        function move(index, nextIndex) {
            if ($parent.attr("data-scrolling") == '1') {
                return false;
            }
            $parent.attr("data-scrolling", '1');

            $parent.attr("data-index", index).find(".slide-text-left-title .item-title-wrap").css({
                top: -index * viewHeight
            })

            //slide content trái
            $parent.find(".slide-text-left-content .item-content-wrap").css({
                left: -index * viewLeftWidth
            });

            //chuyển số của slide trái
            let newNumber = $("<div class='number-new'>").html(('0' + (index + 1)).slice(-2));
            let links = $parent.attr("data-index", index).find(".slide-text-left-title .item-title-wrap .item")
            if (links.length && links[index]) {
                let link = links[index]
                let href = $(link).attr('data-href')
                $('.readmore.readmore-red a').attr('href', href)
            }

            $parent.find(".slide-text-left-number").append(newNumber)
            $parent.find(".slide-text-left-number .number-old").fadeOut(300);
            $parent.find(".slide-text-left-number .number-new").fadeIn(300, function () {
                $parent.find(".slide-text-left-number .number-old").remove();
                $(this).removeClass("number-new").addClass("number-old");
            })

            //slide title ô bên phải
            $parent.find(".slide-text-right-title .item-title-wrap").css({
                left: -index * viewRightWidth
            });

            //chuyển số slide phải
            newNumber = $("<div class='number-new'>").html(('0' + (nextIndex + 1)).slice(-2));
            $parent.find(".slide-text-right-number").append(newNumber);
            $parent.find(".slide-text-right-number .number-old").fadeOut(300);
            $parent.find(".slide-text-right-number .number-new").fadeIn(300, function () {
                $parent.find(".slide-text-right-number .number-old").remove();
                $(this).removeClass("number-new").addClass("number-old");
                $parent.attr("data-scrolling", '0');
            })
        }
    })

}

$("document").ready(function () {
    slideText();

    $(window).resize(function () {
        slideText();
    })
})
